require 'test_helper'

class CreationsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @creation = creations(:orange)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Creation.count' do
      post creations_path, params: { creation: { title: "Lorem ipsum" } }
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Creation.count' do
      delete creation_path(@creation)
    end
    assert_redirected_to login_url
  end
end
