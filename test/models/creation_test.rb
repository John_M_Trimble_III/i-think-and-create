require 'test_helper'

class CreationTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  # 

  def setup
    @user = users(:michael)
    # This code is not idiomatically correct.
    @creation = @user.creations.build(title: 'Lorem ispum', summary: "Lorem ipsum", user_id: @user.id)
  end

  test "should be valid" do
    assert @creation.valid?
  end

  test "user id should be present" do
    @creation.user_id = nil
    assert_not @creation.valid?
  end  


  test "summary should be present" do
    @creation.summary = "   "
    assert_not @creation.valid?
  end

  test "title should be present" do
    @creation.title = "   "
    assert_not @creation.valid?
  end

  test "order should be most recent first" do
    assert_equal creations(:most_recent), Creation.first
  end  

  

end
