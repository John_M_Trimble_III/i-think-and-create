module CommentsHelper
	def nested_comments(comments, isnested = false)
  	comments.map do |comment, sub_comment|
    		render(comment) + nested_comments(sub_comment)
  	end.join.html_safe
	end
end
