module ImpressionsHelper
	def impression_count
    impressions.size
  end

  def unique_impression_count
    impressions.group(:ip_address).size #UNTESTED: might not be correct syntax
  end

  def log_impression
		impressionable_class = controller_name.gsub("Controller", "").singularize.capitalize.constantize

		impressionable_instance = impressionable_class.find(params[:id])
		
    if current_user  
      impressionable_instance.impressions.create(ip_address:request.remote_ip, user_id:current_user.id)
    else
      impressionable_instance.impressions.create(ip_address:request.remote_ip)
    end
  end

end
