module MessagesHelper
	require 'github/markup'

  # Returns HTML from input text using GitHub-flavored Markdown.
  def markdown_to_html(text)
		GitHub::Markup.render('README.md', text)
  end

end
