class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :log_impression, :only => [:show]

  def show
    @user = User.find(params[:id])
    @creations = @user.creations.paginate(:page => params[:creations_page])
    @liked = @user.creations_liked.paginate(:page => params[:liked_page]);
    @following = @user.creations_following.paginate(:page => params[:following_page]);
  end

   # GET /users/:id/edit
  def edit
   # authorize! :update, @user
  end

	def index
		@users= User.all.paginate(:page => params[:page])
	end

 # PATCH/PUT /users/:id.:format
  def update
    # authorize! :update, @user
    respond_to do |format|
      if @user.update(user_params)
        sign_in(@user == current_user ? @user : current_user, :bypass => true)
        format.html { redirect_to @user, notice: 'Your profile was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET/PATCH /users/:id/finish_signup
  def finish_signup
    # authorize! :update, @user 
    if request.patch? && params[:user] && params[:user][:email]
      current_user.skip_reconfirmation!
      if current_user.update(user_params)
        sign_in(current_user, :bypass => true)
        redirect_to current_user, notice: 'Your profile was successfully updated.'
      else
        @show_errors = true
      end
    end
  end

  # DELETE /users/:id.:format
  def destroy
    # authorize! :delete, @user
    @user.destroy
    respond_to do |format|
      format.html { redirect_to root_url }
      format.json { head :no_content }
    end
  end
  
  private
    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      accessible = [ :name, :email, :avatar, :banner ] # extend with your own params
      accessible << [ :password, :password_confirmation ] unless params[:user][:password].blank?
      accessible << [:website, :location, :bio]
      params.require(:user).permit(accessible)
    end
end