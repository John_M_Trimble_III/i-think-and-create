class ConversationsController < ApplicationController
	before_action :authenticate_user!
	
	def new
    @conversation = Conversation.new
    @conversation.user_conversations.build
    @conversation.messages.build

    respond_to do |format|
      commentPartial = ( render :partial => 'new' )
      format.js {commentPartial.to_json}
    end
  end


  def create
    # ugly as shit and I hate this, but it works. We are doing it this way because we need rails to create a unique entry for every 
    # option selected in the multiselect this doesn't work for default native rails.
    users = conversation_params[:user_conversations_attributes]["0"][:user_id]  
    
    users = users.reject { |c| c.empty? }.map(&:to_i)
    
    users << current_user.id

    users = users.sort

    existingConvo = Conversation.find_by_users_hash(users.to_s)

    unless existingConvo
      @conversation = Conversation.create()

      users.each do |user_id| 
        @conversation.user_conversations.build({user_id: user_id.to_i})
      end

      @conversation.messages.build({body: conversation_params[:messages_attributes]["0"][:body], user_id: current_user.id})
      if @conversation.save

        conversation_block = render_conversation(@conversation)

        @conversation.users.each do|user|
        
          ActionCable.server.broadcast "conversation_channel_user_#{user.id}",
                                   body: conversation_block,
                                   type: "list",
                                   inner_text: @conversation.users.where.not(id: user.id).map(&:name).to_sentence,
                                   unread_messages_count: "<span class=\"badge\">#{UserConversation.find_by_conversation_id_and_user_id(@conversation.id, user.id).unread_messages_count}</span>",
                                   is_sending_user: current_user.id == user.id

        end

        head :ok
      end
    else

      existingConvo.messages.build({body: conversation_params[:messages_attributes]["0"][:body], user_id: current_user.id})
      existingConvo.save    

      
    end

  end

  def show
  	@conversation = Conversation.find(params[:id])
  	respond_to do |format|
      format.js {}
      format.html {redirect_to action: "index", :id => params[:id]}
		end
  end

  def index
  	@conversations = current_user.conversations
    
  end

  def edit
  end

  def update
  end


	protected
	def conversation_params	
		params.require(:conversation).permit( messages_attributes: [:body], user_conversations_attributes: [user_id: []])
	end

  def render_conversation(conversation)
    render(partial: 'conversation', locals: { conversation: conversation })
   end  

end
