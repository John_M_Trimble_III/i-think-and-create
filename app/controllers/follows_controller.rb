class FollowsController < ApplicationController
	before_action :set_project
  
  def create
    if Follow.create(followed: @followed, user: current_user)
      flash[:notice] = 'Project has been followed'
      redirect_to @followed
    else
      flash[:alert] = 'Something went wrong...*sad panda*'
      redirect_to @followed
    end
  end
  
  def destroy
    Follow.where(followed_id: @followed.id, user_id: current_user.id).first.destroy
    flash[:notice] = 'Project is no longer in favorites'
    redirect_to @followed
  end
  
  private
  
  def set_project
  	@followed = params[:followed_type].constantize.find(params[:followed_id])
  end
end
