class ChallengesController < ApplicationController

  def new
  	@challenge = Challenge.new
  end


  def create
    @challenge = Challenge.new(challenge_params)    
    if @challenge.save
    	redirect_to @challenge
    else
      render 'new'
  	end
  end

  def show
  	@challenge = Challenge.find(params[:id])
  end

  def index
    @challenges = Challenge.all
  end

	protected
	def challenge_params
		params.require(:challenge).permit(:name, :description, :challenge_picture, :deadline)
	end

end


