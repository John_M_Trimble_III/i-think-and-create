class SearchController < ApplicationController
	def index
	  @results = ThinkingSphinx.search(ThinkingSphinx::Query.escape(params[:search]))
	end
end
