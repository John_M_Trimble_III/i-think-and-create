class CreationsController < ApplicationController
  require 'zip'
  before_action :correct_user, only: [:destroy]
  before_action :admin_user, only: [:destroy]
  before_action :log_impression, :only => [:show]



  def new
    @user = current_user
    @creation = Creation.new
    @creation.user_files.build
  end


  def create
    @creation = current_user.creations.build(creation_params)
    # Sync User File to creation_id first
    if @creation.save
      if params[:user_files]
        params[:user_files].each_with_index { |id, index|
          UserFile.find(id).update_columns(:creation_id => @creation.id, :position => index);
        }
      end
      flash[:success] = "Creation uploaded!"
      redirect_to current_user
    else
      flash[:danger] = @creation.errors.full_messages
      render 'new'
    end
  end

  def edit
    @creation = Creation.find(params[:id])
  end

  def update
    @creation = Creation.find(params[:id])
    if @creation.update_attributes(creation_params)
      if params[:user_files]
        params[:user_files].each_with_index { |id, index|
          UserFile.find(id).update_columns(:position => index);
        }
      end      
      flash[:success] = "Creation updated"
      redirect_to @creation
    else
      render 'edit'
    end
  end



  def index
    @creations = Creation.paginate(page: params[:page])
  end 

  def show
    @creation = Creation.find(params[:id])
    if current_user
      @new_comment    = Comment.build_from(@creation, current_user.id, "")
    else
      @new_comment    = Comment.build_from(@creation, nil, "")
    end

  end

  def destroy
  end

  def download
    @creation = Creation.find(params[:creation_id])

    #Attachment name
    filename = 'basket_images-'+Time.now.to_s.gsub(/[^0-9]/,'')+'.zip'
    temp_file = Tempfile.new(filename)
     
    begin
      #This is the tricky part
      #Initialize the temp file as a zip file
      Zip::OutputStream.open(temp_file) { |zos| }
     
      #Add files to the zip file as usual
      Zip::File.open(temp_file.path, Zip::File::CREATE) do |zip|
        #Put files in here
        @creation.user_files.each do |user_file|
          zip.add(user_file.file_identifier, user_file.file.path )
        end
      end
     
      #Read the binary data from the file
      zip_data = File.read(temp_file.path)
     
      #Send the data to the browser as an attachment
      #We do not send the file directly because it will
      #get deleted before rails actually starts sending it
      send_data(zip_data, :type => 'application/zip', :filename => filename)
    ensure
      #Close and delete the temp file
      temp_file.close
      temp_file.unlink
      @creation.increment!(:num_downloads)

    end
  end




  def fork 
    @creation = Creation.find(params[:creation_id])
    @curr_user = current_user
    @source_user = @creation.user
    @dupe_creation = @creation.dup
    @dupe_creation.user_id = current_user.id
    @dupe_creation.num_downloads = 0

    if @dupe_creation.save
      if @creation.user_files
        @creation.user_files.each { |a|           
        
        }


      end
    end
    redirect_to @dupe_creation
  end



  



  private
    def creation_params
      params.require(:creation).permit(:title, :summary, :file, :category, :category_id, tag_ids: [])
    end

end



