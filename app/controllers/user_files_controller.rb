class UserFilesController < ApplicationController
# truncated for brevity.
  def create

		@userFile = UserFile.new(file_params)
		respond_to do |format|
    	if(@userFile.save(validate: false))
		    format.json { render json: @userFile, :status => :ok }
  		end
		end
  end

  def destroy
    UserFile.find(params[:id]).destroy
    respond_to do |format|
    	format.js 
    end
  end      

  protected

  def file_params
    params.require(:creation).permit(:file, :creation_id, :position)
  end  

end