class CategoriesController < ApplicationController

	def index 
	end
	def new
	end
	def create
		@category = Category.new(category_params)
    if @category.save
      flash[:success] = "Category Created"
				redirect_to action: "index"
    else
      redirect_toredirect_to action: "index"
    end
	end


	private
		def category_params 
			params.require(:category).permit(:name, :description, :parent_id)
		end
end