class MessagesController < ApplicationController
	def create
		@message = Message.new(message_params)
		@message.user_id = current_user.id
		@message.conversation_id = params[:conversation_id]
		if @message.save
			ActionCable.server.broadcast "conversation_channel_" + params[:conversation_id],
                                   body:  render_message(@message), 
                                   sender_id: current_user.id,
                                   type: "message"
      head :ok

		else

		end
	end

	protected
	def message_params
		params.require(:message).permit(:body)
	end

	def render_message(message)
  	render(partial: 'message', locals: { message: message })
   end

end
