class LikesController < ApplicationController
	before_action :set_project
  
  def create
    if Like.create(liked: @liked, user: current_user)
      flash[:notice] = 'Project has been liked'
      redirect_to @liked
    else
      flash[:alert] = 'Something went wrong...*sad panda*'
      redirect_to @liked
    end
  end
  
  def destroy
    Like.where(liked_id: @liked.id, user_id: current_user.id).first.destroy
    flash[:notice] = 'Project is no longer liked'
    redirect_to @liked
  end
  
  private
  
  def set_project
  	@liked = params[:liked_type].constantize.find(params[:liked_id])
  end

end


