ThinkingSphinx::Index.define :creation, :with => :real_time do
  # fields
  indexes title, :sortable => true
  indexes summary
  indexes category.name, :as => :user, :sortable => true
  indexes tag.name

  # attributes
  has user_id,  :type => :integer
  has created_at, :type => :timestamp
  has updated_at, :type => :timestamp
end