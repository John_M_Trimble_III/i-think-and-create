# encoding: utf-8

class FileUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  version :thumb, :if => :image? do
    process :resize_to_fit => [200, 200]
  end




  version :json, :if => :threed_file? do
    process :create_json
    def full_filename(for_file)
      super(for_file).chomp(File.extname(super(for_file))) + '.json'
    end
  end

  version :json_thumb, from_version: :json, :if => :threed_file? do
    process :create_json_thumb
    process :resize_to_fit => [200, 200]

    def full_filename(for_file)
      super(for_file).chomp(File.extname(super(for_file))) + '.png'
    end
  
  end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  # def extension_white_list
  #   %w(jpg jpeg gif png)
  # end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end
  # 
  # 
  # 
  


  protected
    @@threedFileTypes = ['fbx','dae','gltf', 'glb','blend','3ds','ase','obj','ifc','xgl','zgl','ply','dxf','lwo','lws','lxo','stl','x','ac','ms3d','cob','scn','bvh','csm','xml','irrmesh','irr','mdl','md2','md3','pk3','mdc','md5','smd','vta','ogex', '3d','b3d','q3d','q3s','nff','off','raw','ter','mdl','hmp','ndo']


    def image?(new_file)
      new_file.content_type.start_with? 'image'
    end

    def threed_file?(new_file)
      @@threedFileTypes.include? new_file.extension.downcase
    end

  private
    def create_json
      cache_stored_file! if !cached?
      system( "assimp2json " + current_path + " " +  current_path)
    end

    def create_json_thumb
      # Find some way to get file /uploads/user_file/file/102/json_d2m-cap.json
      cache_stored_file! if !cached?
      system("node " + Rails.root.join('vendor','backend3dRenderer','threeDRenderer.js').to_s + " " + current_path + " " + current_path)

    end


end
