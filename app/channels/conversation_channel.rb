# Be sure to restart your server when you modify this file. Action Cable runs in a loop that does not support auto reloading.
class ConversationChannel < ApplicationCable::Channel
  def subscribed
    # stream_from "some_channel"
    UserConversation.find_by_user_id_and_conversation_id(current_user.id, params[:conversation_id]).update(unread_messages_count: 0)
    stream_from "conversation_channel_#{params[:conversation_id]}"
    stream_from "conversation_channel_user_#{current_user.id }"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
