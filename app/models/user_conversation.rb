class UserConversation < ApplicationRecord
  belongs_to :user
  belongs_to :conversation
  has_many :messages, :through => :conversation
  after_create :put_users_in_users_hash

  def put_users_in_users_hash
  	conversation = self.conversation
  	conversation.users_hash = conversation.user_conversations.pluck(:user_id).sort
  	conversation.save
  end

end
