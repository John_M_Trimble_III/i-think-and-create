class Message < ApplicationRecord
  belongs_to :user
  belongs_to :conversation
  after_save :increment_unread_messages_on_user_conversations


  def increment_unread_messages_on_user_conversations
  	# message = self
  	unless(conversation.user_conversations.empty?)
  		conversation = self.conversation
  		user_conversations = conversation.user_conversations
  	
  		sending_user = self.user

  		clean_conversations = []

  		user_conversations.each do |user_conversation|
  			clean_conversations << user_conversation unless user_conversation.user_id == sending_user.id
  		end
  		clean_conversations.each do |clean_conversation|
  			saved_user = clean_conversation.increment(:unread_messages_count)
  			saved_user.save
  		end
  	end
  end
end
