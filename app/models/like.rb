class Like < ApplicationRecord
  belongs_to :user
  belongs_to :like
  belongs_to :liked, polymorphic: true
end
