class Category < ApplicationRecord
	has_many :creations
	has_ancestry :orphan_strategy => :adopt
end
