class Tag < ApplicationRecord
	has_many :taggings
	has_many :creations, through: :taggings

end
