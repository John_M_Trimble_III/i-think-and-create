class Creation < ApplicationRecord
  after_save ThinkingSphinx::RealTime.callback_for(:creation)

  ratyrate_rateable "rating"

	default_scope -> { order(created_at: :desc) }  
  belongs_to :user
  belongs_to :category

  has_many :impressions, as: :impressionable, dependent: :destroy
  has_many :user_files, dependent: :destroy
	accepts_nested_attributes_for :user_files, allow_destroy: true
	
  has_many :taggings
	has_many :tags, through: :taggings

  has_many :likes, as: :liked

  validates :user_id, presence: true
  validates :summary, presence: true
  validates :title, presence: true

  acts_as_commentable


end
