class UserFile < ApplicationRecord
  belongs_to :creation
	#validations
	validates :creation, presence: true  
	validates :file, presence: true
  # Avatar uploader using carrierwave
  mount_uploader :file, FileUploader  

  default_scope { order(position: :asc) }

end



