class User < ApplicationRecord
  ratyrate_rater

  after_save ThinkingSphinx::RealTime.callback_for(:user)


  TEMP_EMAIL_PREFIX = 'change@me'
  TEMP_EMAIL_REGEX = /\Achange@me/

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :omniauthable,
         :confirmable, :validatable
         

  has_many :authentications, :dependent => :destroy
  has_many :creations, dependent: :destroy
  has_many :comments
  has_many :impressions, :as=>:impressionable, dependent: :destroy


  has_many :friendships, :dependent => :destroy
  has_many :friends, -> { where(friendships: {status: "accepted"} ) }, :through => :friendships
  has_many :requested_friends, -> { where(friendships: {status: "requested"} ).order(created_at: :desc) },  :through => :friendships, :source => :friend
  has_many :pending_friends,  -> { where(friendships: {status: "pending"} ).order(created_at: :desc) },  :through => :friendships, :source => :friend
  
  has_many :likes
  has_many :creations_liked, through: :likes, source: :liked, source_type: 'Creation'
  
  has_many :follows
  has_many :users_following, through: :follows, source: :followed, source_type: "User"
  has_many :creations_following, through: :follows, source: :followed, source_type: "Creation"

  has_many :user_conversations
  has_many :conversations, :through => :user_conversations
  has_many :messages, :through => :conversations


  mount_uploader :avatar, FileUploader
  mount_uploader :banner, FileUploader


  validates :name, presence: true, length: { maximum: 50}







  def self.find_for_oauth(auth, signed_in_resource = nil)

    # Get the identity and user if they exist
    authentication = Authentication.find_for_oauth(auth)

    # If a signed_in_resource is provided it always overrides the existing user
    # to prevent the identity being locked with accidentally created accounts.
    # Note that this may leave zombie accounts (with no associated identity) which
    # can be cleaned up at a later date.
    user = signed_in_resource ? signed_in_resource : authentication.user

    # Create the user if needed
    if user.nil?

      # Get the existing user by email if the provider gives us a verified email.
      # If no verified email was provided we assign a temporary email and ask the
      # user to verify it on the next step via UsersController.finish_signup
      email_is_verified = auth.info.email && (auth.info.verified || auth.info.verified_email)
      email = auth.info.email if email_is_verified
      user = User.where(:email => email).first if email

      # Create the user if it's a new registration
      if user.nil?
        user = User.new(
          name: auth.extra.raw_info.name,
          #username: auth.info.nickname || auth.uid,
          email: email ? email : "#{TEMP_EMAIL_PREFIX}-#{auth.uid}-#{auth.provider}.com",
          password: Devise.friendly_token[0,20]
        )
        user.skip_confirmation!
        user.save!
      end
    end

    # Associate the authentication with the user if needed
    if authentication.user != user
      authentication.user = user
      authentication.save!
    end
    user
  end


  def email_verified?
    self.email && self.email !~ TEMP_EMAIL_REGEX
  end
end
