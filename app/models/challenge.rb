class Challenge < ApplicationRecord

	has_many :challenge_entries
	has_many :entries, through: :challenge_entries

	mount_uploader :challenge_picture, FileUploader


end
