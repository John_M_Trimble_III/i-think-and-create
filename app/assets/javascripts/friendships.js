$(function(){ // dom ready

	$(document).on('ajax:success', '.new_friendship', function(e, response, status ) {
		$('body .container').prepend(response);
		$('#new_friendship').replaceWith('<span class="label label-primary">Friend Request Sent</span>');
	});

	$(document).on('ajax:error', '.new_friendship', function(e, response, status ) {
		$('body .container').prepend(response.responseText);
	});

});
