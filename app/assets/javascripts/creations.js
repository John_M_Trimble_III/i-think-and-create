// Show Action
// 
var newFiles = [];
var deleteFiles = [];

$(document).on('turbolinks:load', function() {

  // Creations Show
  if($('.creations.show').length){
    
    $('#myCarousel').carousel({
      pause: true,
      interval: false
    });

    $('#myCarousel').on('slide.bs.carousel',function(e){
      var slideFrom = $(this).find('.active').index();
      var slideTo = $(e.relatedTarget).index();
      $( `#thumbnail-indicators li:nth-child(${slideFrom + 1})` ).toggleClass("active");
      $( `#thumbnail-indicators li:nth-child(${slideTo + 1})` ).toggleClass("active");    
    });

    $('#creation_tag_ids').chosen();
    $('#creation_category_id').chosen();

    if($('.threed-model-container').length > 0){
        // Animation for 3d file
      var canvas;
      var camera, renderer;
      var scenes = [];
      var material, mesh;
      init();
      animate();
    }
  }

  // Creations New
  if($('.creations.new').length || $('.creations.edit').length){
    // fileUpload('#creation_files_file');
    $('#creation_files_file').on('change', handleFileSelect);
    var el = document.getElementById('upload-list');
    window.sortable = Sortable.create(el, {    // Changed sorting within list
      onUpdate: function (evt) {
        reIndexFileList();
        // + indexes from onEnd
      }
    });
  }
  
function handleFileSelect(evt) {
  var newStartPosition = $('.list-group-item.item-upload-image-list').length; // If empty this is 0
  evt.stopPropagation();
  evt.preventDefault();
  var files = evt.target.files; // FileList object
  files = files || evt.dataTransfer.files; // FileList object.

  // files is a FileList of File objects. List some properties.
  var output = [];
  
  for (var i = 0, f; f = files[i]; i++) {
    var id = 'file_list_item_' + i;
    // Only process image files.
    var initPosition = i + newStartPosition;

    var listItem = $('\
      <li class="list-group-item item-upload-image-list" >\
        <div class="file-container">\
          <div class="file-position">\
            <span class="badge position">' + (initPosition) + '</span>\
          </div>\
          <div class="file-thumbnail">\
          </div>\
          <div class="file-info">\
            <strong>' + escape(f.name) + '</strong></br>\
            <p>' + (f.size / (1024*1024)).toFixed(2) + ' MB</p>\
          </div>\
          <div class="file-actions">\
            <a class="glyphicon glyphicon-trash"></a>\
          </div>\
        </div>\
          <div class="progress">\
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">\
            </div>\
          </div>\
        </li>');

    $('#upload-list').append(listItem);


    listItem.find('.glyphicon.glyphicon-trash').click(function(){
      if(confirm("Are you sure you want to remove this item?")) {
        $.ajax({
          context: $(this).parents('li'),
          url: '/user_files/' + $(this).parents('li').find('input').val(),
          type: 'DELETE',
          success: function(data) {
            //called when successful            
            this.remove();
            $('.item-upload-image-list').find('.position').each(function(index, value){
              $(value).html(index);
            });
            return true;
          },
          error: function(e) {
          //called when there is an error
          //console.log(e.message);
            return false;
          }
        });
      }
    });


    var data = new FormData();    
    data.append('creation[file]', f);
    if(window.creation_id){
      data.append('creation[creation_id]', window.creation_id);
    }
    data.append('creation[position]', initPosition);

    $.ajax({
      url: "/user_files",
      context: listItem,
      cache: false,
      data: data,
      type: 'POST',
      synchron: false,
      contentType: false, // Set content type to false as jQuery will tell the server its a query string request
      processData: false, // Don't process the files
      xhr: function() {
        var xhr = new window.XMLHttpRequest();
        var self = this;

        xhr.upload.addEventListener("progress", function(evt) {
          fileUploadProgress(evt,self.context.find('.progress-bar'));
        }, false);

        xhr.addEventListener("progress", function(evt) {
          // fileDownloadProgress(evt,self.context.find('.progress-bar'));
        }, false);

        return xhr;
      },
      success: function(data){

        this.append('<input type="hidden" name="user_files[]" value="' + data.id + '" />');      

        this.find('.progress').fadeOut("slow", function(){
          $('<div class="text-success"> Success</div>').hide().appendTo(this.parentElement).fadeIn("slow");
        });
      
      },
      error: function(el){

        var parent = el.find(".jFiler-jProgressBar").parent();
        el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
          $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");
        });

      },
      statusCode: null,
      onProgress: function(){},
      onComplete: function(){}
    });




    if (f.type.match('image.*')) {
      // Closure to capture the file information.  
      var reader = new FileReader();
      reader.onload = (function(theFile, id, el) {
        return function(e) {
          // // Render thumbnail.
          var image = document.createElement('image');
          image.innerHTML = ['<img class="upload-thumb" src="', e.target.result, '" title="', escape(theFile.name), '"/>'].join('');
          el.find('.file-thumbnail').prepend(image);
          // document.getElementById(id).insertBefore(span, null);
          // var imageList = $('#'+id);
        };
      })(f,id, listItem);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    
    } else {
      var ext = f.name.split('.').pop(); // Extension
      listItem.find('.file-thumbnail').append('<div class="file-icon"><span class="glyphicon glyphicon-file"><strong>.' + ext + '</strong></span></div>');
    }

  }

  reIndexFileList();

};


function reIndexFileList() {
  $('.item-upload-image-list').find('.position').each(function(index, value){
    $(value).html(index);
  });
}


function fileUploadProgress(evt, el) {
  if (evt.lengthComputable) {
    var percentComplete = evt.loaded / evt.total;
    el.css("width", percentComplete * 100 + '%');
    //Do something with upload progress here
  }
}

function fileDownloadProgress(evt, el) {
  if (evt.lengthComputable) {
    var percentComplete = evt.loaded / evt.total;
    el.css("width", percentComplete * 100 + '%');
    //Do something with download progress
  }
}

function handleFileSelectOnEdit(evt) {
  evt.stopPropagation();
  evt.preventDefault();

  var files = evt.target.files; // FileList object
  files = files || evt.dataTransfer.files; // FileList object.

  // files is a FileList of File objects. List some properties.
  var output = [];
  
  for (var i = 0, f; f = files[i]; i++) {

      var id = 'file_list_item_' + i;

      // Only process image files.
      if (f.type.match('image.*')) {
      // Closure to capture the file information.
      // 
      var reader = new FileReader();
      
       reader.onload = (function(theFile, id) {
        return function(e) {
          // // Render thumbnail.
          var image = document.createElement('image');
          image.innerHTML = ['<img class="upload_thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          $('#'+id).find('.upload_thumbnail').prepend(image);
          // document.getElementById(id).insertBefore(span, null);
          // var imageList = $('#'+id);

        };
      })(f,id);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    
    }

    var liText = "<li class='list-group-item item-upload-image-list' id='"+id+"'><div class='upload_thumbnail'></div><div><strong>"+escape(f.name)+"</strong> ("+ (f.type || "n/a")+ ") - "+
                f.size+ " bytes, last modified: "+
                f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString() : "n/a"+
                "</div><span class='badge position'>"+ i+"</span></li>";
    var tempText = "<li class='list-group-item image-list' id='" + id + "'>"+escape(f.name)+"<span class='badge position'></span></li>";

    $("#upload-list").append(tempText);

  }

    $('.list-group-item.image-list').find('.position').each(function(index, value){
      $(value).html(index);
    });


  // var el = document.getElementById('upload-list');
  // var sortable = Sortable.create(el, {    // Changed sorting within list
  //   onUpdate: function (evt) {
  //       var itemEl = evt.item;  // dragged HTMLElement
  //       $('.item-upload-image-list').find('.position').each(function(index, value){
  //         $(value).html(index);
  //       });
  //       // + indexes from onEnd
  //   },});


}



function handleDragOver(evt) {
  evt.stopPropagation();
  evt.preventDefault();
  evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
}

function handleFileDrop(e) {
  var files = e.target.files, filesLength = files.length;

  if(!e.target.files) return;

  for (var i = 0; i < filesLength; i++) {

    var f = files[i];
    newFiles.push(f);
    var fileReader = new FileReader();

    fileReader.onload = (function(f,index) {
      return function(e) {
        var elImageItem = $('.list-group.uploaded-image-list').append('\
          <li class="list-group-item" data-index="' + index + '"">\
            <div>\
              <span class="badge position">' + index + '</span>\
              <div class="image-thumb">\
                <img class="imageThumb" src="' + e.currentTarget.result + '" title="' + f.name + '"/>\
              </div>\
              <div class="file-details">\
                <div class="file-name">\
                  ' + f.name + '\
                </div>\
                <div class="file-type">\
                  ' + f.type + '\
                </div>\
              </div>\
              <a class="glyphicon glyphicon-trash pull-right"></a>\
            </div>\
          </li>');

      $(elImageItem.find('.glyphicon.glyphicon-trash.pull-right').get(index)).click(function(){
        if(confirm("Are you sure you want to remove this item?")) {
          this.parentElement.parentElement.remove();
          delete newFiles[$(this.parentElement.parentElement).data('index')];
        }
      });
    }
    })(f,i);
    // End Of Onload
    fileReader.readAsDataURL(f);
  }
  
  var el = document.getElementsByClassName('uploaded-image-list')[0];
  var sortable = Sortable.create(el, {    // Changed sorting within list
    onUpdate: function (evt) {
      var itemEl = evt.item;  // dragged HTMLElement
      $('.list-group-item').find('.position').each(function(index, value){
          $(value).html(index);
      });
      // + indexes from onEnd
    },
  });
}
});
