var previousLink = null;
var loadByRefresh = true;
function subscribeToConversation(conversation_id) {
  App.conversation = App.cable.subscriptions.create({channel: 'ConversationChannel', conversation_id: conversation_id}, {
    connected: function() {
      // Called when the subscription is ready for use on the server
    },

    disconnected: function() {
      // Called when the subscription has been terminated by the server
    },

    received: function(data) {
      // Called when there's incoming data on the websocket for this channel
      // If my current user id doesn't match the sender id change the class id
      if(data.type == "message")
      {
        var message = data.body;

        if($('#chat').data('currentuser-id') !== data.sender_id) {
          message = $(message).removeClass('me').addClass('you');
        }

        $('div.current-chat-area ul.current-chat').append(message);

        scrollBottom();
      }
      else if(data.type == "list")
      {
        var conversation = data.body;

        conversation = $(conversation).html(data.inner_text);

        $('div.conversations div.list-group').append(conversation);

        var convoCounter = $('div.conversations h3 span');
        convoCounter.html(parseInt(convoCounter.html())+1);

        $(conversation).append(data.unread_messages_count);

        if(data.is_sending_user)
        {
          $(conversation).click();
        }
      }
      else
      {
        alert("Sorry you are not supposed to see this.");
      }
    }
  });
}

$(document).on('turbolinks:load', function() {
  $('.conversation-submit.btn.btn-large.btn-warning').click(function(e){
    $('.current-conversation').html("");
  });

  $('body').on('click', '.conversation-load-link', function() {
    if(previousLink) previousLink.removeClass('active');
    previousLink = $(this);
    $(this).addClass('active');
    $(this).find('span.badge').html("");
  });


  $(window).on("popstate", function (event) {
    loadByRefresh = false;
    if (id = $.urlParam('id')) {
      // For Every Link See If One Matches
      // I need to do this without clicking
      // Otherwise it triggers the pushstate
      $('.conversation-load-link').each(function(e) {
        var self = this;
        if( id == this.href.split('/').pop()) {
          self.click();
        }
      });
    }
  }); 

	$('div.conversations').on('ajax:success', '.conversation-load-link', function(e, response, status ) {    

    if(App.conversation) App.conversation.unsubscribe();
    
    subscribeToConversation(this.href.split('/').pop());
		
    $('div.current-conversation').html(response);
        
    // Render Obviously But We Also Need To Subscribe To This New Channel
    // So perhaps connect first so we can receive messages
    // window.location.href
    if(loadByRefresh) window.history.pushState(null, this.innerText, location.origin + '/conversations' + '?id=' + this.href.split('/').pop());
    resizeChat();
    $('#conversation_user_conversations_attributes_0_user_id').chosen({    width: "auto"});

	});		

	$('div.conversations').on('ajax:error', '.conversation-load-link',  function(e, response, status ) {
		alert("Woops you weren't supposed to see that. Move along, nothing to see here. ");
	});

  $('body').on('keydown', '#message_body', function(event) {

  	if(13 == event.keyCode)
  	{
			$('#new_message input').click();
			event.target.value = "";
			event.preventDefault();
  	}

  });

  $( window ).resize(resizeChat);


  // Instead of Just the First Click Click a Differnt One

  if (id = $.urlParam('id')) {
    // For Every Link See If One Matches
    $('.conversation-load-link').each(function(e) {
      var self = this;
      if( id == this.href.split('/').pop()) {
        self.click();
      }
    });

  } else {
    $('div.conversations a.conversation-load-link').first().click();
  }


});

// Utility Functions
var resizeChat = function() {
  var submitHeight = $('div.row.current-chat-footer').outerHeight();
  var navbarheight = $('nav').outerHeight();
	var newHeight = $(window).height();

  $('div.current-chat-area').height(newHeight - submitHeight - navbarheight);
  $('div.conversations').height(newHeight - navbarheight);
  $('div.current-conversation').height(newHeight - navbarheight);
  scrollBottom();
}

var scrollBottom = function(){
	$('.row.current-chat-area').scrollTop($('.row.current-chat-area')[0].scrollHeight);
}


$.urlParam = function(name) {
  var results = new RegExp('[\?&amp;]' + name + '=([^&amp;#]*)').exec(window.location.href);
  return results? results[1]  : null;
}
