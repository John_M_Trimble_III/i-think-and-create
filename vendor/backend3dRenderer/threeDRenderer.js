if(!frontend()) {
  width  = 750;
  height = 400;
  THREE = require("three");
  ASSIMPJSONLOADER = require("three/examples/js/loaders/AssimpJSONLoader");
  PNG   = require('pngjs').PNG
  gl    = require("gl")(width, height, { preserveDrawingBuffer: true });
  fs    = require('fs');
  png    = new PNG({ width: width, height: height })

  var canvas;
  var camera, renderer;
  var scene;
  var material, mesh;
  init();
  }


  function init() {
      // Make Canvas macth up with carousel-inner
      
      scenes = [];

      if (frontend()){
        canvas = document.getElementById( "c" );
        $("canvas").css({ width:$('.carousel-inner').width() , height: $('.carousel-inner').height()});

        $('.threed-model-container').each(function( index, modelContainer ) {
          
          var fileUrl = $(modelContainer).data("url"); // Needs to be changed if run from command line
          var loader = new THREE.AssimpJSONLoader();
          loader.load(fileUrl, function(object) {objectLoaded(object,index);}, onProgress, onError );

          scene = new THREE.Scene();
      
          scene.userData.element = modelContainer;

          camera = new THREE.PerspectiveCamera( 45, window.innerWidth/window.innerHeight, 1, 10000 );
          camera.position.set(250,250,250);

          scene.userData.camera = camera;
          scene.userData.controls = createController(modelContainer);
          scene.userData.controls.maxDistance = 9500;
          
          addLights();

          addWorldLayout();
        
          scenes.push( scene );
        });

      renderer = new THREE.WebGLRenderer( { preserveDrawingBuffer: true  ,canvas: canvas, antialias: true, alpha: true  } );
      renderer.setClearColor( 0x000000, 0 ); // the default
      renderer.setPixelRatio( window.devicePixelRatio );
      
      } else {

        var fileUrl = process.argv.slice(2)[0]; // Needs to be changed if run from command line
        path = process.argv.slice(2)[1];
        console.log('File Url: ' + fileUrl);
        console.log('Path: ' + path);
        var loader = new THREE.AssimpJSONLoader();
        loader.load(fileUrl, function(object) { objectLoaded(object); }, onProgress, onError );

        // # THREE.js business starts here
        scene = new THREE.Scene()

        // # camera attributes
        VIEW_ANGLE = 45
        ASPECT = width / height
        NEAR = 0.1
        FAR  = 10000

        // # set up camera
        camera = new THREE.PerspectiveCamera(VIEW_ANGLE, ASPECT, NEAR, FAR);

        scene.add(camera)
        camera.position.set(250,250,250);
        camera.lookAt(scene.position)

        // # mock object, not used in our test case, might be problematic for some workflow
        canvas = new Object()


        gl.clearColor(1, 1, 1, 1)


        // // # The width / height we set here doesn't matter
        renderer = new THREE.WebGLRenderer({
            antialias: true,
            width: 0,
            height: 0,
            canvas: canvas, 
            context: gl,
            alpha: true
        })

        // // # add some geometry

        addWorldLayout();
        
        addLights();
        // // # Let's create a render target object where we'll be rendering

        rtTexture = new THREE.WebGLRenderTarget(
            width, height, {
                minFilter: THREE.LinearFilter,
                magFilter: THREE.NearestFilter,
                format: THREE.RGBAFormat
        })

        renderer.setClearColor(0xffffff, 1 );
        
      }      
  }

  function updateSize() {
    // Resize Canvas To Parent Size
    $(canvas).width(canvas.parentElement.offsetWidth);
    $(canvas).height(canvas.parentElement.offsetHeight);

    var width = canvas.clientWidth;
    var height = canvas.clientHeight;
    
    scenes.forEach( function( scene ) {
      scene.userData.controls.handleResize();
    });

    if ( canvas.width !== width || canvas.height != height ) {
      renderer.setSize( width, height, false );
    }
  }



  function animate() {

    render();
    requestAnimationFrame( animate );
    scenes.forEach( function( scene ) {
      scene.userData.controls.update();
    });
  
  }


  function render() {


        updateSize();

        renderer.setScissorTest( false );
        renderer.clear();

        renderer.setScissorTest( true );

        scenes.forEach( function( scene ) {

          // so something moves
          scene.children[0].rotation.y = Date.now() * 0.001;

          // get the element that is a place holder for where we want to
          // draw the scene
          var element = scene.userData.element;

          // get its position relative to the page's viewport
          var rect = element.getBoundingClientRect();
  
          var parentEl = $(element).parent('div');

          // check if it's offscreen. If so skip it
          if ( parentEl.hasClass('active') ||  parentEl.hasClass('next') || parentEl.hasClass('prev')) {


            // set the viewport
            var width  = rect.right - rect.left;
            var height = rect.bottom - rect.top;
            var left   = $(element).offset().left - $(canvas).offset().left;
            var bottom = 0;

            renderer.setViewport( left, bottom, width, height );
            renderer.setScissor( left, bottom, width, height );

            var camera = scene.userData.camera;

            //camera.aspect = width / height; // not changing in this example
            //camera.updateProjectionMatrix();

            //scene.userData.controls.update();

            renderer.render( scene, camera );
          }

          return;
        } );

  }

  var objectLoaded = function(object, sceneId){
      // object.scale.multiplyScalar( 0.25 );
      object.children[0].material = new THREE.MeshPhongMaterial({color: 0xf2642f})
      
      if(frontend()){
        scenes[sceneId].add( object );
      } else {
        scene.add(object);

      }

      object.rotateX(-1*Math.PI/2);

      //reposition To New Location
      var bbox = new THREE.Box3();
      bbox.setFromObject(object);
      var cVec = bbox.getCenter();
      // This is Good the Z axis is now the distance from the plane
      object.translateX(cVec.x * -1);
      object.translateY(cVec.z);
      object.translateZ(bbox.min.y * -1);
      bbox.setFromObject(object);

      if(frontend()){
        scenes[sceneId].userData.controls.target.set(bbox.getCenter().x, bbox.getCenter().y, bbox.getCenter().z);
      } else {
      
        renderer.render(scene, camera, rtTexture, true);
        // # read render texture into buffer
        gl = renderer.getContext();

        // # create a pixel buffer of the correct size
        var pixels = new Uint8Array(4 * width * height);

        // # read back in the pixel buffer
        gl.readPixels(0, 0, width, height, gl.RGBA, gl.UNSIGNED_BYTE, pixels);

        var a, b, g, i, j, k, m, r, stream;
        for (j = 0; j <= height; j++) {
          for (i = 0; i <= width; i++) {
            k = j * width + i;
            r = pixels[4 * k];
            g = pixels[4 * k + 1];
            b = pixels[4 * k + 2];
            a = pixels[4 * k + 3];
            m = (height - j + 1) * width + i;
            png.data[4 * m] = r;
            png.data[4 * m + 1] = g;
            png.data[4 * m + 2] = b;
            png.data[4 * m + 3] = a;
          }
        }
        stream = fs.createWriteStream(path);
        png.pack().pipe(stream);
        stream.on('close', function() {
          return console.log("Image written: " + path);
        });
      
      }
  }

  var onProgress = function ( xhr ) {
    if ( xhr.lengthComputable ) {
      var percentComplete = xhr.loaded / xhr.total * 100;
      console.log( Math.round(percentComplete, 2) + '% downloaded' );
    }
  };

  var onError = function ( xhr ) {
  };

  function createController(domElement){
    var controls = new THREE.TrackballControls( camera, domElement );
    controls.noZoom = false;
    controls.noPan = true;

    controls.staticMoving = false;
    controls.dynamicDampingFactor = .2;
    
    controls.addEventListener( 'change', render );
    return controls;
  }

  function addLights(){
    var light = new THREE.DirectionalLight( 0xffffff, 1 );
    light.position.set( 1, 1, 1 );

    scene.add( light );
    hemiLight = new THREE.HemisphereLight( 0xffffff, 0xffffff, 0.6 ); 
    scene.add(hemiLight);
  }

  // Tell me whether I'm coding for node or frontend
  function frontend() {
    return typeof window !== 'undefined';
  }

  function addWorldLayout() {
    var helper = new THREE.GridHelper( 200, 10 );
    helper.setColors( 0x0000ff, 0x808080 ); // blue central line, gray grid
    scene.add( helper );

    var axisHelper = new THREE.AxisHelper( 100 );
    scene.add( axisHelper );
    }