Rails.application.routes.draw do
  post '/rate' => 'rater#create', :as => 'rate'
  resources :softwares
  resources :printers
  devise_for :users, :controllers => { registrations: 'registrations', omniauth_callbacks: 'omniauth_callbacks'}
  resources :users, :only => [:show]

  get 'password_resets/new'

  get 'password_resets/edit'

  root      'static_pages#home'
  get       '/help',    to: 'static_pages#help'
  get       '/about',   to: 'static_pages#about'
  get       '/contact', to: 'static_pages#contact'
  get       '/search', to: 'search#index'
	
  match '/users/:id/finish_signup' => 'users#finish_signup', via: [:get, :patch], :as => :finish_signup


  
  resources :users do 
    resources :creations, shallow: true
    resources :friendships
  end


  resources :comments

  resources :creations, shallow: true do 
    get :download
    get :fork
  end

  resources :user_files, only: [:destroy, :create]

  resources :categories

  resources :likes, only: [:create,:destroy]
  
  resources :follows, only: [:create,:destroy]

  resources :account_activations, only: [:edit]


  resources :challenges

  resources :conversations, shallow: true do
    resources :messages
  end
  
  mount ActionCable.server, at: '/cable'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
