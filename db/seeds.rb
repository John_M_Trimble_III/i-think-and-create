# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)



User.create!(name:  "Example User",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true
             )


99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
              password_confirmation: password
              )
end


users = User.order(:created_at).take(6)
50.times do
  title = Faker::Lorem.sentence(5)
  summary = Faker::Lorem.sentence(5)

  users.each { |user| user.creations.create!(title: title, summary: summary) }
end


Category.create(name: '3D Printer Parts')
Category.create(name: 'Art')
Category.create(name: 'Contest')
Category.create(name: 'Education')
Category.create(name: 'Fashion')
Category.create(name: 'Home & Garden')
Category.create(name: 'Tech')
Category.create(name: 'Video Games')
Category.create(name: 'Board Games')
Category.create(name: 'Jewelry')
Category.create(name: 'DIY')
Category.create(name: 'Miniatures')
Category.create(name: 'Other')
Category.create(name: 'Toys')



Printer.create(name: "LulzBot TAZ 6 3D Printer" , url:  "https://www.amazon.com/LulzBot-TAZ-6-3D-Printer/dp/B01DLU3M3M/ref=sr_1_1?s=industrial&ie=UTF8&qid=1493331106&sr=1-1-spons&keywords=3d+printer&psc=1")
Printer.create(name: "JGAURORA 3d Printer", url: "https://www.amazon.com/JGAURORA-Printers-Professional-Resolution-Education/dp/B015DQV4CS/ref=sr_1_2?s=industrial&ie=UTF8&qid=1493331106&sr=1-2-spons&keywords=3d+printer&psc=1")
Printer.create(name: "Monoprice Select Mini 3D Printer" , url: "https://www.amazon.com/Monoprice-Select-Printer-Heated-Filament/dp/B01FL49VZE/ref=sr_1_3?s=industrial&ie=UTF8&qid=1493331106&sr=1-3&keywords=3d+printer")
Printer.create(name: "Monoprice 13860 Maker", url: "https://www.amazon.com/Monoprice-13860-Maker-Select-Printer/dp/B018GZBC3Y/ref=sr_1_4?s=industrial&ie=UTF8&qid=1493331106&sr=1-4&keywords=3d+printer")
Printer.create(name: "FlashForge 3D Printers, New Model: Finder", url:  "https://www.amazon.com/FlashForge-3D-Printers-New-Model/dp/B016R9E7J2/ref=sr_1_5?s=industrial&ie=UTF8&qid=1493331106&sr=1-5&keywords=3d+printer")
Printer.create(name: "XYZprinting da Vinci Jr.", url:  "https://www.amazon.com/XYZprinting-Vinci-Jr-1-0-Printer/dp/B01MYO02BA/ref=sr_1_6?s=industrial&ie=UTF8&qid=1493331106&sr=1-6&keywords=3d+printer")
Printer.create(name: "2016 Newest High Performance Shining 3D Einstart-S", url:  "https://www.amazon.com/Performance-Einstart-S-Framework-Accuracy-Stability/dp/B01MR62A6D/ref=sr_1_7?s=industrial&ie=UTF8&qid=1493331106&sr=1-7&keywords=3d+printer")
Printer.create(name: "MakerBot Replicator+", url:  "https://www.amazon.com/MakerBot-MP07825-Replicator/dp/B01LTIA9Y8/ref=sr_1_8?s=industrial&ie=UTF8&qid=1493331106&sr=1-8&keywords=3d+printer")
Printer.create(name: "XYZprinting da Vinci mini 3D Printer", url:  "https://www.amazon.com/XYZprinting-Vinci-mini-Printer-5-9/dp/B01IXVXV9Y/ref=sr_1_9?s=industrial&ie=UTF8&qid=1493331106&sr=1-9&keywords=3d+printer")
Printer.create(name: "ALUNAR 3D Desktop Printer Prusa i3 DIY High Accuracy CNC Self Assembly", url:  "https://www.amazon.com/ALUNAR-Desktop-Printer-Accuracy-Assembly/dp/B018XJ3E02/ref=sr_1_10?s=industrial&ie=UTF8&qid=1493331106&sr=1-10&keywords=3d+printer")
Printer.create(name: "FlashForge 3d Printer Creator Pro", url:  "https://www.amazon.com/FlashForge-Structure-Optimized-Platform-Extruder/dp/B00I8NM6JO/ref=sr_1_11?s=industrial&ie=UTF8&qid=1493331106&sr=1-11&keywords=3d+printer")
Printer.create(name: "DIY RepRap Prusa I3 V2", url:  "https://www.amazon.com/REPRAPGURU-RepRap-Printer-Plastic-Company/dp/B01KD6T7Z4/ref=sr_1_12?s=industrial&ie=UTF8&qid=1493331106&sr=1-12&keywords=3d+printer")
Printer.create(name: "Sindoh 3DWOX DP200 3D Printer", url:  "https://www.amazon.com/Sindoh-3DWOX-DP200-3D-Printer/dp/B017IZBFB2/ref=sr_1_13?s=industrial&ie=UTF8&qid=1493331106&sr=1-13&keywords=3d+printer")
Printer.create(name: "Sindoh 3DWOX DP201 3D Printer", url:  "https://www.amazon.com/Sindoh-3DWOX-DP201-3D-Printer/dp/B01N35J1BQ/ref=sr_1_14?s=industrial&ie=UTF8&qid=1493331106&sr=1-14&keywords=3d+printer")
Printer.create(name: "QIDI TECHNOLOGY X-ONE 3D Printer", url:  "https://www.amazon.com/TECHNOLOGY-X-ONE-Printer-Structure-Touchscreen/dp/B01HZ4HY9I/ref=sr_1_15?s=industrial&ie=UTF8&qid=1493331106&sr=1-15&keywords=3d+printer")
Printer.create(name: "Anycubic Linear Version Unassemble Delta Rostock 3D", url:  "https://www.amazon.com/Anycubic-Version-Unassemble-Rostock-Printer/dp/B019VSIANU/ref=sr_1_16?s=industrial&ie=UTF8&qid=1493331106&sr=1-16&keywords=3d+printer")
Printer.create(name: "QIDI TECHNOLOGY 3DP-QDA16-01 Dual Extruder Desktop 3D Printer", url:  "https://www.amazon.com/TECHNOLOGY-3DP-QDA16-01-Extruder-Structure-Filaments/dp/B01D8M32LU/ref=sr_1_17?s=industrial&ie=UTF8&qid=1493331106&sr=1-17&keywords=3d+printer")
Printer.create(name: "HICTOP Filament Monitor Desktop 3D Printer", url:  "https://www.amazon.com/Arrival-HICTOP-Filament-Self-assembly-Printing/dp/B01ERDWSFU/ref=sr_1_18?s=industrial&ie=UTF8&qid=1493331106&sr=1-18&keywords=3d+printer")
Printer.create(name: "New Matter MOD-t 3D Printer", url:  "https://www.amazon.com/New-Matter-MOD-t-3D-Printer/dp/B01HBZYFT8/ref=sr_1_17?s=industrial&ie=UTF8&qid=1493332011&sr=1-17&keywords=3d+printer")
Printer.create(name: "ALUNAR® Upgraded DIY Desktop 3D Printer Reprap Prusa i3 Kit", url:  "https://www.amazon.com/Upgraded-Accuracy-Self-Assembly-Tridimensional-Multicolor/dp/B01H6QEPOM/ref=sr_1_18?s=industrial&ie=UTF8&qid=1493332011&sr=1-18&keywords=3d+printer")
Printer.create(name: "HICTOP 3D Printer Prusa I3 DIY Kit", url:  "https://www.amazon.com/HICTOP-Printer-Prusa-Aluminum-300x300x400mm/dp/B06XSF5MGT/ref=sr_1_19?s=industrial&ie=UTF8&qid=1493332011&sr=1-19&keywords=3d+printer")
Printer.create(name: "DIY RepRap Prusa I3 3D Printer Kit", url:  "https://www.amazon.com/REPRAPGURU-Black-DIY-Prusa-Printer/dp/B01E06IHJ0/ref=sr_1_20?s=industrial&ie=UTF8&qid=1493332011&sr=1-20&keywords=3d+printer")
Printer.create(name: "Dremel Idea Builder 3D Printer", url:  "https://www.amazon.com/Dremel-Idea-Builder-3D-Printer/dp/B00NA00MWS/ref=sr_1_24?s=industrial&ie=UTF8&qid=1493332011&sr=1-24&keywords=3d+printer")
Printer.create(name: "New 3D Printer Inventor Pro by Instone 3D Printing", url:  "https://www.amazon.com/3D-Inventor-Instone-Printing-Maintenance/dp/B06WRSX16L/ref=sr_1_25?s=industrial&ie=UTF8&qid=1493332011&sr=1-25-spons&keywords=3d+printer&psc=1")

Software.create(name: "Cura")
Software.create(name: "Cura")
Software.create(name: "CraftWare")
Software.create(name: "123D Catch")
Software.create(name: "3D Slash")
Software.create(name: "TinkerCad ")
Software.create(name: "3DTin")
Software.create(name: "Sculptris")
Software.create(name: "ViewSTL")
Software.create(name: "Netfabb Basic")
Software.create(name: "Repetier")
Software.create(name: "FreeCAD")
Software.create(name: "SketchUp")
Software.create(name: "3D-Tool Free Viewer")
Software.create(name: "Meshfix")
Software.create(name: "Simplify3D")
Software.create(name: "Blender")
Software.create(name: "MeshLab")
Software.create(name: "Meshmixer")
Software.create(name: "OctoPrint")







