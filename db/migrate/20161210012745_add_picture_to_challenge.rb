class AddPictureToChallenge < ActiveRecord::Migration[5.0]
  def change
    	add_column :challenges, :challenge_picture, :string
  end
end
