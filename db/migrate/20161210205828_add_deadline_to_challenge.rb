class AddDeadlineToChallenge < ActiveRecord::Migration[5.0]
  def change
  	add_column :challenges, :deadline, :datetime
  end
end
