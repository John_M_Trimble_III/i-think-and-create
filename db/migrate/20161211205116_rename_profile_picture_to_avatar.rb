class RenameProfilePictureToAvatar < ActiveRecord::Migration[5.0]
  def change
  	rename_column :users, :profile_picture, :avatar
  end
end
