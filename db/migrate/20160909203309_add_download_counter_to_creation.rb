class AddDownloadCounterToCreation < ActiveRecord::Migration[5.0]
  def change
    	add_column :creations, :num_downloads, :integer
  end
end
