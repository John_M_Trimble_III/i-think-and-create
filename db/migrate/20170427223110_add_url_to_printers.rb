class AddUrlToPrinters < ActiveRecord::Migration[5.0]
  def change
  	add_column :printers, :url, :string
  end
end
