class CreateChallengeentries < ActiveRecord::Migration[5.0]
  def change
    create_table :challengeentries do |t|
      t.references :creation, foreign_key: true
      t.references :challenge, foreign_key: true

      t.timestamps
    end
  end
end
