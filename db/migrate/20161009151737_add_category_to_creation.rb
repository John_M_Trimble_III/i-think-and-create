class AddCategoryToCreation < ActiveRecord::Migration[5.0]
  def change
  	add_reference :creations, :category, index: true
  end
end
