class AddAcceptedAtToFriendship < ActiveRecord::Migration[5.0]
  def change
  	add_column :friendships, :accepted_at, :date
  end
end
