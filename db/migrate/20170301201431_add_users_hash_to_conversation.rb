class AddUsersHashToConversation < ActiveRecord::Migration[5.0]
  def change
  	add_column :conversations, :users_hash, :string
  end
end
