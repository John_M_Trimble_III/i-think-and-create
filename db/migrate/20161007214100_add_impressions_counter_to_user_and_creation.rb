class AddImpressionsCounterToUserAndCreation < ActiveRecord::Migration[5.0]
  def change
		add_column :users, :impressions_count, :integer
		add_column :creations, :impressions_count, :integer
  end
end
