class CreatCommentsModel < ActiveRecord::Migration[5.0]
    create_table :comments do |t|
      t.text :body
      t.references :commentable, polymorphic: true
      t.references :user, foreign_key: true
      t.string :ancestry

      t.timestamps
    end

		add_index :comments, :ancestry
end
