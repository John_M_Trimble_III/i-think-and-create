class RemoveColumnsFromUserConversations < ActiveRecord::Migration[5.0]
  def change
  	remove_column :user_conversations, :deleted
  	remove_column :user_conversations, :read
  	add_column :user_conversations, :unread_messages_count, :integer,  :default => 0
  end
end
