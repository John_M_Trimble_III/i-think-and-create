class AddOAuthTokenAndOAuthExpiration < ActiveRecord::Migration[5.0]
  def change
  	add_column :authentications, :oauth_token, :text
  	add_column :authentications, :oauth_expires_at, :datetime
  end
end
