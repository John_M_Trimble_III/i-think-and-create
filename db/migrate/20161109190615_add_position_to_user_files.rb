class AddPositionToUserFiles < ActiveRecord::Migration[5.0]
  def change
  	add_column :user_files, :position, :integer
  end
end
